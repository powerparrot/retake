const express = require('express');
const path = require('path');
let app = express();
let htmlpath = path.join(__dirname, '/public');

app.use(express.static(htmlpath));

let server = app.listen(8080, function () {
    var host = 'localhost';
    var port = server.address().port;
    console.log('listening on http://' + host + ':' + port + '/');
});