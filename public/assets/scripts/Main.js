import { AssetManager } from './AssetManager.js';
//import { ClassManager } from './ClassManager.js';
import { ShipManager } from './ShipManager.js';
import { Inputs } from './Inputs.js';


let config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    parent: document.getElementById('game'),
    physics: {
        default: 'arcade',
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};
let game = new Phaser.Game(config);
let ship;
let bullets;
let classes;
let Bullet;
let startTime = Date.now();
let bulletTime = 0;
let bulletSpeed = 400;
let inputs;
function preload() {
    AssetManager.loadAssets(this);
    inputs = Inputs.getShipInputs(Phaser, this);
}
function create() {
    Bullet = new Phaser.Class({

        Extends: Phaser.Physics.Arcade.Sprite,

        initialize:

            function Bullet(scene) {
                Phaser.Physics.Arcade.Sprite.call(this, scene, 0, 0, 'bullet');
            },

        fire: (bullet, ship, speed) => {
            console.log(this);
            bullet.setPosition(ship.x, ship.y - 65);
            bullet.setScale(0.25, 0.3);
            bullet.setActive(true);
            bullet.setVisible(true);
            bullet.body.velocity.y = (speed * (-1));
        },

        update: () => {
            console.log('update');
            if (this.y < 0) {
                this.setActive(false);
                this.setVisible(false);
                console.log('bullet inactive')
            }
        }
    });
    this.add.image(400, 300, 'sky');
    //classes = ClassManager.loadClasses(Phaser, this);
    ship = ShipManager.createShip(this);
    bullets = this.add.group({
        classType: Bullet,
        maxSize: 30,
        runChildUpdate: true
    });
    console.log(bullets);
    startTime = Date.now();
}
function update() {
    let cur = ShipManager.updateShip(inputs, ship, bullets, bulletSpeed, curTime(), bulletTime);
    if (cur) {
        bulletTime = curTime() + 130;
    }
}
function fireBullet(scene) {
    if (curTime() > bulletTime) {
        bullet = scene.physics.add.sprite(ship.x, ship.y - 65, 'bullet');
        bullet.setScale(0.25, 0.3);
        bullet.body.velocity.y = (bulletSpeed * (-1));
        bulletTime = curTime() + 130;
        removeBullet(bullet, function recieved() { console.log('bullet removed') });
    }

}
function createBullets(game) {
    let group = game.add.group({
        classType: Bullet,
        maxSize: 30,
        runChildUpdate: true
    });
    console.log(group);
    return group;
}
function resetBullet(bullet) {
    bullet.kill();
}
function removeBullet(bullet, callback) {
    setTimeout(function () { bullet.destroy(); callback(); }, (ship.y / bulletSpeed) * 1000);
}
function curTime() {
    return (parseInt(Date.now()) - startTime);
}

const times = [];
let fps;

function refreshLoop() {
    window.requestAnimationFrame(() => {
        const now = performance.now();
        while (times.length > 0 && times[0] <= now - 1000) {
            times.shift();
        }
        times.push(now);
        fps = times.length;
        document.getElementById('fps').innerHTML = 'FPS: ' + fps;
        refreshLoop();
    });
}

refreshLoop();
