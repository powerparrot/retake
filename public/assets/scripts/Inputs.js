export const Inputs = {
    getShipInputs: (Phaser, game) => {
        let fire = game.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
        let curs = game.input.keyboard.createCursorKeys();
        let inputs = {
            cursorKeys: curs,
            fireKey: fire
        };
        return inputs;
    }
};