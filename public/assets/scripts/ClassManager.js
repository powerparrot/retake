let bullet;

export const ClassManager = {
    loadClasses: (Phaser, game) => {
        let Bullet = new Phaser.Class({

            Extends: Phaser.GameObjects.Sprite,

            initialize:

                function Bullet() {
                    //Phaser.GameObjects.Image.call(this, scene, 0, 0, 'bullet');
                    bullet = game.physics.add.sprite(400, 530, 'bullet');
                },

            fire: (ship, speed) => {
                bullet.setPosition(ship.x, ship.y - 65);
                bullet.setScale(0.25, 0.3);
                bullet.setActive(true);
                bullet.setVisible(true);
                bullet.body.velocity.y = (speed * (-1));
            },

            update: () => {
                console.log('update');
                if (this.y < 0) {
                    this.setActive(false);
                    this.setVisible(false);
                    console.log('bullet inactive')
                }
            }
        });
        const classes = {
            BulletClass: Bullet
        };
        return Bullet;
    }
}