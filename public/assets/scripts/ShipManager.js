export const ShipManager = {
    createShip: (game) => {
        let ship = game.physics.add.sprite(400, 530, 'ship', 'Shipboi1.png');
        ship.setCollideWorldBounds(true);
        var shipFrames = game.anims.generateFrameNames('ship', {
            start: 1, end: 4, zeroPad: 1,
            prefix: 'Shipboi', suffix: '.png'
        });
        game.anims.create({ key: 'thruster', frames: shipFrames, frameRate: 15, repeat: -1 });
        ship.anims.play('thruster');
        return ship;
    },
    updateShip: (inputs, ship, bullets, speed, time, bulletTime) => {
        ship.body.velocity.x = 0;
        ship.body.velocity.y = 0;
        if (inputs.cursorKeys.left.isDown) {
            ship.body.velocity.x = -300;
        }
        if (inputs.cursorKeys.right.isDown) {
            ship.body.velocity.x = 300;
        }
        if (inputs.fireKey.isDown) {
            if (time > bulletTime) {
                let bullet = bullets.get();
                if (bullet) {
                    bullet.fire(bullet, ship, speed);
                    return true;
                }
            }
        }
    },
}