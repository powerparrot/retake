export const AssetManager = {
    loadAssets: (game) => {
        game.load.setBaseURL('http://localhost:8080');
        game.load.atlas('ship', 'assets/sprites/ships/playership/Ship.png', 'assets/sprites/ships/playership/Ship.json');
        game.load.image('sky', 'assets/sprites/backgrounds/background.png');
        game.load.image('bullet', 'assets/sprites/ammo/Bullet.png');
    }
}